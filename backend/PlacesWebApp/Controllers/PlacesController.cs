﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PlacesWebApp.Data;
using PlacesWebApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlacesWebApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PlacesController : ControllerBase
    {
        private readonly IDataRepository _dataRepository;

        public PlacesController(IDataRepository dataRepository)
        {
            _dataRepository = dataRepository;
        }

        [HttpGet]
        public async Task<IEnumerable<Place>> GetAllPlaces()
        {
            return await _dataRepository.GetAllPlaces();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Place>> GetById(int id)
        {
            var place = await _dataRepository.GetById(id);
            if (place == null)
            {
                return NotFound();
            }
            return place;
        }

        [HttpGet("paged")]
        public async Task<PagedResults> GetPlaces(int pageIndex = 1, int pageSize = 10)
        {
            return await _dataRepository.GetPagedPlaces(pageIndex, pageSize);
        }

        [HttpGet("countries")]
        public async Task<IEnumerable<Country>> GetCountries()
        {
            return await _dataRepository.GetCountries();
        }

        [HttpPost]
        public async Task<ActionResult<Place>> InsertPlace(PlacePostRequest postRequest)
        {
            var savedPlace = await _dataRepository.InsertPlace(postRequest);
            return CreatedAtAction(nameof(GetById), new { id = savedPlace.Id }, savedPlace);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<Place>> UpdatePlace(int id, PlacePutRequest putRequest)
        {
            var place = await _dataRepository.GetById(id);
            if (place == null)
            {
                return NotFound();
            }
            putRequest.Name = string.IsNullOrEmpty(putRequest.Name) ? place.Name : putRequest.Name;
            putRequest.PostCode = string.IsNullOrEmpty(putRequest.PostCode) ? place.PostCode : putRequest.PostCode;
            putRequest.Country = string.IsNullOrEmpty(putRequest.Country) ? place.Country : putRequest.Country;

            return await _dataRepository.UpdatePlace(id, putRequest);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeletePlace(int id)
        {
            var place = _dataRepository.GetById(id);
            if (place == null)
            {
                return NotFound();
            }
            await _dataRepository.DeletePlace(id);
            return NoContent();
        }
    }
}
