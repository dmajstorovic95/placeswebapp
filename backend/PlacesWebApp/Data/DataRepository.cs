﻿using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using PlacesWebApp.Data.Models;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlacesWebApp.Data
{
    public class DataRepository : IDataRepository
    {
        private readonly string _connectionString;

        public DataRepository(IConfiguration configuration)
        {
            _connectionString = configuration["ConnectionStrings:DefaultConnection"];
        }

        public async Task<IEnumerable<Place>> GetAllPlaces()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync();
                return await connection.QueryAsync<Place>(@"EXEC dbo.spPlace_GetAllNotPaged");
            }
        }

        public async Task<Place> GetById(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync();
                return await connection.QueryFirstAsync<Place>(@"EXEC dbo.spPlace_GetPlaceById @Id = @Id", new { Id = id });
            }
        }

        public async Task<PagedResults> GetPagedPlaces(int pageIndex, int pageSize)
        {
            var results = new PagedResults();

            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync();
                using (var multi = await connection.QueryMultipleAsync(@"EXEC dbo.spPlace_GetAll @PageIndex = @PageIndex, @PageSize = @PageSize; 
                                                                        EXEC dbo.spPlace_TotalCount", 
                                                                        new { PageIndex = pageIndex, PageSize = pageSize }))
                {
                    results.Places = multi.Read<Place>().ToList();
                    results.TotalCount = multi.ReadFirst<int>();
                }
            }

            return results;
        }

        public async Task<IEnumerable<Country>> GetCountries()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync();
                return await connection.QueryAsync<Country>(@"EXEC dbo.spCountry_GetAll");
            }
        }

        public async Task<Place> InsertPlace(PlacePostRequest postRequest)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync();
                var placeId = await connection.QueryFirstAsync<int>(@"EXEC dbo.spPlace_Insert @Name = @Name, @PostCode = @PostCode, @Country = @Country", 
                                                new { Name = postRequest.Name, PostCode = postRequest.PostCode, Country = postRequest.Country });
                return await GetById(placeId);
            }
        }

        public async Task<Place> UpdatePlace(int id, PlacePutRequest putRequest)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync();
                await connection.ExecuteAsync(@"EXEC dbo.spPlace_Update @Id = @Id, @Name = @Name, @PostCode = @PostCode, @Country = @Country",
                                                new {Id = id, Name = putRequest.Name, PostCode = putRequest.PostCode, Country = putRequest.Country });
                return await GetById(id); 
            }
        }

        public async Task DeletePlace(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync();
                await connection.ExecuteAsync(@"EXEC dbo.spPlace_Delete @Id = @Id",
                                                new { Id = id });
            }
        }

    }
}
