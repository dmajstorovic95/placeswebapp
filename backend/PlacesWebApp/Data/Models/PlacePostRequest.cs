﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PlacesWebApp.Data.Models
{
    public class PlacePostRequest
    {
        [Required]
        [StringLength(150)]
        public string Name { get; set; }

        [Required]
        [StringLength(100)]
        public string PostCode { get; set; }

        [Required]
        [StringLength(100)]
        public string Country { get; set; }
    }
}
