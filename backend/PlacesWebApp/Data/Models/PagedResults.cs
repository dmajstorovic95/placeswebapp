﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlacesWebApp.Data.Models
{
    public class PagedResults
    {
        public IEnumerable<Place> Places { get; set; }
        public int TotalCount { get; set; }
    }
}
