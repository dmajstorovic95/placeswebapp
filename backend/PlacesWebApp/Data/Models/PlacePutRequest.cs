﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PlacesWebApp.Data.Models
{
    public class PlacePutRequest
    {
        [StringLength(150)]
        public string Name { get; set; }

        [StringLength(100)]
        public string PostCode { get; set; }

        [StringLength(100)]
        public string Country { get; set; }
    }
}
