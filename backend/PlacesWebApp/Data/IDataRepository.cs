﻿using PlacesWebApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlacesWebApp.Data
{
    public interface IDataRepository
    {
        Task<IEnumerable<Place>> GetAllPlaces();

        Task<Place> GetById(int id);

        Task<PagedResults> GetPagedPlaces(int pageIndex, int pageSize);

        Task<IEnumerable<Country>> GetCountries();

        Task<Place> InsertPlace(PlacePostRequest postRequest);

        Task<Place> UpdatePlace(int id, PlacePutRequest putRequest);

        Task DeletePlace(int id);
    }
}
