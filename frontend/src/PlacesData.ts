export interface Place {
  id: number;
  name: string;
  postCode: string;
  country: string;
}

export interface FormData {
  name: string;
  postCode: string;
  country: string;
}

export interface PlaceData {
  places: Array<Place>;
  totalCount: number;
}
export interface Country {
  countryID: number;
  countryName: string;
  twoCharCountryCode: string;
  threeCharCountryCode: string;
}

export const getPagedPlaces = async (
  pageIndex: number,
  pageSize: number,
): Promise<PlaceData> => {
  let pagedPlaces: PlaceData;
  const response = await fetch(
    `https://localhost:5001/api/places/paged?pageIndex=${pageIndex}&pageSize=${pageSize}`,
  );
  pagedPlaces = await response.json();
  return pagedPlaces;
};

export const savePlace = (formData: Place) => {
  if (!formData.id) {
    postPlace(formData);
  } else {
    updatePlace(formData);
  }
};

export const postPlace = async (formData: Place) => {
  const data: FormData = {
    name: formData.name,
    postCode: formData.postCode,
    country: formData.country,
  };
  await fetch('https://localhost:5001/api/places', {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: data ? JSON.stringify(data) : undefined,
  });
};

export const updatePlace = async (formData: Place) => {
  const data: FormData = {
    name: formData.name,
    postCode: formData.postCode,
    country: formData.country,
  };
  await fetch(`https://localhost:5001/api/places/${formData.id}`, {
    method: 'PUT',
    headers: { 'Content-Type': 'application/json' },
    body: data ? JSON.stringify(data) : undefined,
  });
};

export const deletePlace = async (id: number) => {
  await fetch(`https://localhost:5001/api/places/${id}`, {
    method: 'DELETE',
  });
};

export const getCountries = async (): Promise<Country[]> => {
  const response = await fetch('https://localhost:5001/api/places/countries');
  return await response.json();
};
