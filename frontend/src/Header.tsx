import React from 'react';
import { PageHeader } from 'antd';

export const Header = () => {
  return <PageHeader title="PlacesWebApp" />;
};
