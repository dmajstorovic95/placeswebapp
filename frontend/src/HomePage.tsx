import React, { useEffect, useState, useCallback } from 'react';
import { Table, Divider, Typography, Button, Row, Col } from 'antd';
import { AddNewForm } from './AddNewForm';
import {
  PlaceData,
  getPagedPlaces,
  Place,
  deletePlace,
  savePlace,
} from './PlacesData';
import { useForm } from 'antd/lib/form/Form';

const { Title } = Typography;

const PAGE = 1;
const PAGESIZE = 10;

export const HomePage = () => {
  const [visible, setVisible] = useState<boolean>(false);
  const [data, setData] = useState<PlaceData>();
  const [form] = useForm();

  const doGetPagedPlaces = useCallback(
    async (page: number, pageSize: number) => {
      const pagedPlaces = await getPagedPlaces(page, pageSize);
      setData(pagedPlaces);
    },
    [],
  );

  useEffect(() => {
    doGetPagedPlaces(PAGE, PAGESIZE);
  }, [doGetPagedPlaces]);

  const handleOnSave = (place: Place) => {
    savePlace(place);
    setVisible(false);
    doGetPagedPlaces(PAGE, PAGESIZE);
  };

  const handleModify = (place: Place) => {
    form.setFieldsValue(place);
    setVisible(true);
  };

  const handleAddNew = () => {
    form.resetFields();
    setVisible(true);
  };

  const handleDelete = (id: number) => {
    deletePlace(id);
    window.location.reload();
  };

  return (
    <div style={{ width: '70%', margin: '0 auto' }}>
      <Row>
        <Col span={8}>
          <Button
            type="primary"
            style={{ float: 'left', margin: '15px 0 0 0' }}
            onClick={() => {
              handleAddNew();
            }}
          >
            Add new
          </Button>
        </Col>
        <Col span={8}>
          <Title>Places</Title>
        </Col>
      </Row>
      <Table
        dataSource={data?.places}
        pagination={{
          total: data?.totalCount,
          showTotal: (total, range) =>
            `${range[0]}-${range[1]} of ${total} items`,
          pageSize: PAGESIZE,
          onChange: (page, pageSize) => doGetPagedPlaces(page, pageSize!),
        }}
      >
        <Table.Column title="Name" dataIndex="name" key="name" />
        <Table.Column title="Postal code" dataIndex="postCode" key="postCode" />
        <Table.Column title="Country" dataIndex="country" key="country" />
        <Table.Column
          title="Action"
          key="action"
          render={(item: Place) => {
            return (
              <span>
                <Button onClick={() => handleModify(item)}>Modify</Button>
                <Divider type="vertical" />
                <Button
                  style={{ color: 'red' }}
                  onClick={() => handleDelete(item.id)}
                >
                  Delete
                </Button>
              </span>
            );
          }}
        />
      </Table>
      <AddNewForm
        form={form}
        visible={visible}
        onSave={handleOnSave}
        onCancel={() => {
          setVisible(false);
        }}
      />
    </div>
  );
};
