import React, { useEffect, useState } from 'react';
import { Form, Input, Modal, Select } from 'antd';
import { FormInstance } from 'antd/lib/form/Form';
import { Country, getCountries, Place, savePlace } from './PlacesData';

interface AddNewFormProps {
  visible: boolean;
  form: FormInstance<any>;
  onSave: (place: Place) => void;
  onCancel: () => void;
}

export const AddNewForm: React.FC<AddNewFormProps> = ({
  visible,
  onSave,
  onCancel,
  form,
}) => {
  const [countries, setCountries] = useState<Country[]>([]);

  useEffect(() => {
    const doGetCountries = async () => {
      const countriesResponse = await getCountries();
      setCountries(countriesResponse);
    };
    doGetCountries();
  }, []);

  const handleSave = () => {
    form
      .validateFields()
      .then((formData) => {
        form.resetFields();
        onSave(formData);
      })
      .catch((info) => {
        console.log('Validate Failed:', info);
      });
  };

  return (
    <Modal
      visible={visible}
      title="Add a new place"
      okText="Save"
      cancelText="Cancel"
      destroyOnClose
      onCancel={onCancel}
      onOk={() => form.submit()}
    >
      <Form
        form={form}
        layout="vertical"
        name="modalform"
        onFinish={() => handleSave()}
      >
        <Form.Item name="id" hidden />
        <Form.Item
          name="name"
          label="Name"
          rules={[{ required: true, message: 'Please enter a name' }]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          name="postCode"
          label="Postal code"
          rules={[{ required: true, message: 'Please enter a postal code' }]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          name="country"
          label="Country"
          rules={[{ required: true, message: 'Please select a country' }]}
        >
          <Select
            placeholder="Select a country"
            optionFilterProp="children"
            showSearch
            filterOption={(input, option) =>
              option?.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
            }
          >
            {countries &&
              countries
                ?.filter((x) => !!x.countryID)
                .map((country) => {
                  return (
                    <Select.Option
                      key={country.countryID}
                      value={country.countryName}
                    >
                      {country.countryName}
                    </Select.Option>
                  );
                })}
          </Select>
        </Form.Item>
      </Form>
    </Modal>
  );
};
